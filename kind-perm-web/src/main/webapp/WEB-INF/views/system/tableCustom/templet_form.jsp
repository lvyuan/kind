<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<title></title>
<%@ include file="/WEB-INF/views/include/easyui.jsp"%>
</head>

<body>
	<div data-options="region: 'center'">
		<div class="container">
			<div class="content">
				<form id="mainformTemplet" action="${ctx}/tableCustom/tableCustom/${action}"
					method="post" novalidate class="form">

					<div class="tabs-panels">
						<div class="panel">
							<div title="" data-options="closable:false"
								class="basic-info panel-body panel-body-noheader panel-body-noborder">
								<div class="column">
									<span class="current">模板信息:</span>
								</div>
								<table class="kv-table" cellspacing="10">
									<input type="hidden" name="id" value="${entity.id }" />
									<tr>
										<td class="kv-label">模板名称：</td>
										<td class="kv-content">
											<input name="name" type="text" value="${entity.name}" class="easyui-validatebox"
											data-options="required:true,validType:['length[0,255]']" />
										</td>
									</tr>
									<tr>
										<td class="kv-label">模板类型：</td>
										<td class="kv-content">
											<input name="type" type="text" value="${entity.type}" class="easyui-validatebox"
											data-options="required:false,validType:['length[0,255]']" />
										</td>
									</tr>
									<tr>
										<td class="kv-label">模板对应的javaBean：</td>
										<td class="kv-content">
											<input name="javaBean" type="text" value="${entity.javaBean}" class="easyui-validatebox"
											data-options="required:false,validType:['length[0,255]']" />
										</td>
									</tr>
									
									
								</table>
							</div>
						</div>
					</div>

				</form>
			</div>
		</div>
	</div>

<script type="text/javascript">
//@ sourceURL=privilege_form.js
	$(function() {
		$('#mainformTemplet').form({
			onSubmit : function() {
				var isValid = $(this).form('validate');
				// 返回false终止表单提交
				return isValid;
			},
			success : function(data) {
				var dataObj = eval("(" + data + ")");//转换为json对象
				if (submitSuccess(dataObj, dgTemplet, dTemplet)) {
					dgTemplet.treegrid('reload');
				}
			}
		});
		
	});
	
</script>
</body>
</html>