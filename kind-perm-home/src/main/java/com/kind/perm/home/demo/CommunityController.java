package com.kind.perm.home.demo;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.kind.common.client.response.JsonResponseResult;
import com.kind.common.contants.Constants;
import com.kind.common.dto.DataGridResult;
import com.kind.common.enums.ResponseState;
import com.kind.common.exception.ServiceException;
import com.kind.common.persistence.PageView;
import com.kind.common.uitls.NumberUtils;
import com.kind.perm.core.demo.domain.CommunityDO;
import com.kind.perm.core.demo.service.CommunityService;
import com.kind.perm.home.common.controller.BaseController;

/**
 * 
 * Function:小区信息管理控制器. <br/>
 * 
 * @date:2016年5月12日 上午11:18:52 <br/>
 * @author 李明
 * @version:
 * @since:JDK 1.7
 */
@Controller
@RequestMapping("demo/community")
public class CommunityController extends BaseController {
	
	@Autowired
	private CommunityService communityService;

    /**列表页面*/
    private final String LIST_PAGE = "demo/community_list";
    /**表单页面*/
    private final String FORM_PAGE = "demo/community_form";

	/**
	 * 进入到默认列表页面
	 */
	@RequestMapping(method = RequestMethod.GET)
	public String toListPage() {
		return LIST_PAGE;
	}

	/**
	 * 获取分页查询列表数据.
	 */
	// @RequiresPermissions("sys:user:view")
	@RequestMapping(value = "selectPageList", method = RequestMethod.GET)
	@ResponseBody
	public DataGridResult selectPageList(CommunityDO query, HttpServletRequest request) {
		PageView<CommunityDO> page = communityService.selectPageList(query);
		return super.buildDataGrid(page);
	}

	/**
	 * 加载添加页面.
	 * 
	 * @param model
	 */
	@RequestMapping(value = "add", method = RequestMethod.GET)
	public String toAdd(Model model) {
		model.addAttribute("entity", new CommunityDO());
		model.addAttribute(Constants.CONTROLLER_ACTION, Constants.CONTROLLER_ACTION_ADD);
		return FORM_PAGE;
	}

	/**
	 * 保存数据.
	 * 
	 * @param entity
	 * @param model
	 */
	@RequestMapping(value = "add", method = RequestMethod.POST)
	@ResponseBody
	public JsonResponseResult add(@Valid CommunityDO entity, Model model) {
        try {
            entity.setCreateDate(new Date());
            communityService.save(entity);
            return JsonResponseResult.success();

        }catch (ServiceException e){
            e.printStackTrace();
            logger.error(e.getMessage());
            return JsonResponseResult.build(ResponseState.ERROR_CODE_SERVICE_ERROR.getCode(), e.getMessage());
        }
	}

	/**
	 * 加载修改页面.
	 * 
	 * @param id
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "update/{id}", method = RequestMethod.GET)
	public String update(@PathVariable("id") Long id, Model model) {
		model.addAttribute("entity", communityService.getById(id));
		model.addAttribute(Constants.CONTROLLER_ACTION, Constants.CONTROLLER_ACTION_UPDATE);
		return FORM_PAGE;
	}

	/**
	 * 修改数据.
	 * 
	 * @param entity
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "update", method = RequestMethod.POST)
	@ResponseBody
	public JsonResponseResult update(@Valid @ModelAttribute @RequestBody CommunityDO entity, Model model) {
        try {
            communityService.save(entity);
            return JsonResponseResult.success();

        }catch (ServiceException e){
            e.printStackTrace();
            logger.error(e.getMessage());
            return JsonResponseResult.build(ResponseState.ERROR_CODE_SERVICE_ERROR.getCode(), e.getMessage());
        }
	}

	/**
	 * 删除数据.
	 * 
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "remove/{id}")
	@ResponseBody
	public JsonResponseResult remove(@PathVariable("id") Long id) {
	    try {
            if (!NumberUtils.isEmptyLong(id)) {
                communityService.remove(id);
            }
            return JsonResponseResult.success();

        }catch (ServiceException e){
            e.printStackTrace();
            logger.error(e.getMessage());
            return JsonResponseResult.build(ResponseState.ERROR_CODE_SERVICE_ERROR.getCode(), e.getMessage());
        }
	}

}
