package com.kind.perm.core.system.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.kind.common.contants.Constants;
import com.kind.common.exception.ServiceException;
import com.kind.perm.core.system.dao.CoFileDao;
import com.kind.perm.core.system.dao.FileDao;
import com.kind.perm.core.system.domain.CoFileDO;
import com.kind.perm.core.system.domain.FileDO;
import com.kind.perm.core.system.service.CoFileService;

/**
 * 
 * 附件关联表. <br/>
 *
 * @date:2016年12月12日 <br/>
 * @author 李明
 * @version:
 * @since:JDK 1.7
 */
@Service
@Transactional(propagation = Propagation.REQUIRED)
public class CoFileServiceImpl implements CoFileService {

	@Autowired
	private CoFileDao cofileDao;
	
	@Autowired
	private FileDao fileDao;
	

	
	@Override
	public int save(CoFileDO entity) throws ServiceException {
		try {
			return cofileDao.saveOrUpdate(entity);

        }catch (Exception e){
		    e.printStackTrace();
            throw new ServiceException(e.getMessage());
        }
	}
	@Override
	public void saveBatch(String [] fileIds,Long typeId,String type)  throws ServiceException {
		try {		
			Date date =new Date();
			for (int i = 0; i < fileIds.length; i++) {
				CoFileDO coFile = new CoFileDO();
				coFile.setObjectA(Constants.COFILE_FILE);
				System.out.println("==="+fileIds[i]);
				if(fileIds[i].indexOf("\"")!=-1)
				{
					fileIds[i] = fileIds[i].replace("\"", "");
				}
				coFile.setObjectAId(Long.parseLong(fileIds[i]));
				coFile.setObjectB(type);
				coFile.setObjectBId(Long.parseLong(typeId+""));
				coFile.setCreateTime(date);
				cofileDao.saveOrUpdate(coFile);
			}

        }catch (Exception e){
		    e.printStackTrace();
            throw new ServiceException(e.getMessage());
        }
	}
	
	@Override
	public void updateBatch(String [] fileIds,Long typeId,String type)  throws ServiceException {
		try {		
			Date date =new Date();
			//1.删除所有t_cofile里，这个id对应的数据
			Map map = new HashMap();
			map.put("objectA", Constants.COFILE_FILE);
			map.put("objectB", type);
			map.put("objectBId", typeId);
			cofileDao.deleteByMap(map);
			//2.然后迭代循环String [] fileIds里面的数据，如果发现有迭代的属性对应的t_file等于null直接跳过；否则针对这些重新添加一遍关联表
			for (int i = 0; i < fileIds.length; i++) {
				System.out.println("==="+fileIds[i]);
				if(fileIds[i].indexOf("\"")!=-1)
				{
					fileIds[i] = fileIds[i].replace("\"", "");
				}
				FileDO filedo = fileDao.getById(Long.parseLong(fileIds[i]));
				if(filedo==null)
				{
					continue;
				}
				CoFileDO coFile = new CoFileDO();
				coFile.setObjectA(Constants.COFILE_FILE);
				coFile.setObjectAId(Long.parseLong(fileIds[i]));
				coFile.setObjectB(type);
				coFile.setObjectBId(Long.parseLong(typeId+""));
				coFile.setCreateTime(date);
				cofileDao.saveOrUpdate(coFile);
			}

        }catch (Exception e){
		    e.printStackTrace();
            throw new ServiceException(e.getMessage());
        }
	}
	
	@Override
	public List<FileDO> getBatch(String objectA,String objectB,Long objectBId) throws ServiceException {
		List<FileDO> filelist = new ArrayList<FileDO>();
		try {
			
			//通过关联文件表找到对应的file的list然后返回
			List<CoFileDO> cofiledolist = cofileDao.getCoFileDOListByTypeBAndTypeBId(objectA, objectB, objectBId);
			for (CoFileDO coFileDO : cofiledolist) {
				FileDO fileDO = fileDao.getById(coFileDO.getObjectAId());
				if(fileDO == null) continue;
				filelist.add(fileDO);
				
			}
			
			return filelist;

        }catch (Exception e){
		    e.printStackTrace();
            throw new ServiceException(e.getMessage());
        }
	}
}
