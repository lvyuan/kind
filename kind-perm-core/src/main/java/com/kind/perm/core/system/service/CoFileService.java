package com.kind.perm.core.system.service;

import java.util.List;

import com.kind.perm.core.system.domain.CoFileDO;
import com.kind.perm.core.system.domain.FileDO;

/**
 * 关联文件关联处理接口<br/>
 *
 * @Date: 2016年12月12日
 * @author 李明
 * @version
 * @since JDK 1.7
 * @see
 */
public interface CoFileService {

    /**
     * 保存数据
     * @param entity
     */
	public int save(CoFileDO entity);
	
	
	/**
	 * 批量添加filed
	 * @param fileIds 批量上传的文件id
	 * @param typeId 类型id
	 */
	public void saveBatch(String[] fileIds,Long typeId,String type);
	
	
	/**
	 * 批量添加filed
	 * @param fileIds 批量上传的文件id
	 * @param typeId 类型id
	 */
	public void updateBatch(String[] fileIds,Long typeId,String type);
    
    
    /**
     * 获得文件的列表
     * @param typeA file类型
     * @param typeB 所属类型
     * @param typeBId 所属的id
     * @return
     */
    public List<FileDO> getBatch(String objectA,String objectB,Long objectBId);



}
