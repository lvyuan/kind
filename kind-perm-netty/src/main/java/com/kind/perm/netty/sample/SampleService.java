package com.kind.perm.netty.sample;

import org.springframework.stereotype.Component;

import com.alibaba.fastjson.JSONObject;

/**
 * sample的service示例
 * User: 李明
 * Date: 2016/1/28
 * Time: 17:09
 * To change this template use File | Settings | File Templates.
 */
@Component
public class SampleService {

    public JSONObject hello(String name) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("code", 6666);
        jsonObject.put("name", name);
        return jsonObject;
    }
}
